# DiffEqParameters.jl
![Development status - alpha](https://img.shields.io/badge/Development_status-pre--alpha-red.svg)


Store and use model parameters together with metadata for reproducibility and convenient filtering.


WARNING: I intend to rename this package soon. The package manager will get confused.

### What is it good for?
This package supplies types for convenient handling of model parameters.
It essentially allows you to create objects which look and feel like arrays (of your parameters), but which also holds additional information. What information is stored is mainly up to you, but one thing that is always stored is a reference to the model that the parameters belong to.


Not only does this package provide `Parameter` types which are designed to contain parameters, but also `ParamCollection` types which contain collections of `Parameter`s. Along with these types comes methods which makes the management of parameter sets easier.

This allows you to essentially have one parameter collection with lots of different parameters, for different models, and still be able to keep everything straight.

To demonstrate this, let's have a look at a minimal example:

### A simple use-case
Let's create a simple model (this can be pretty much anything) and a parameter set:
```julia
julia> using DiffEqParameters
julia> model1(x, p) = p[1]*x^p[2] ## A pretty useless model, but you get the point.
julia> param_values = [10., 2.] ## A normal array.
julia> param1 = Param(model1, param_values )
2-element Param{Float64,typeof(model1)}:
 10.0
  2.0
```

`param1` is now an object which behaves just like the `param_values` array.

```julia
julia> param1[1]
10.0

julia> param1[2]
2.0

julia> [val for val in param1]
[10., 2.0]
```

Ok, so we now have an object which simulates an array of numbers but which takes more code to produce; why should we care? The `param1` object also contains a reference to the model it was made for:

```julia
julia> param1.model
model1 (generic function with 1 method)
```

The parameters "knows" what model it belongs to. This allows us to do filtering in a collection of parameter sets. Let's first create such a collection so that we know what we are dealing with:

```julia
julia> p1 = Param(model1, [1., 2.])
julia> p2 = Param(model1, [3., 4.])
julia> param_collection = ParamCollection([p1, p2])
Parameter Collection
1: [1.0, 2.0] for model: typeof(model1)
2: [3.0, 4.0] for model: typeof(model1)
```

A parameter collection can be indexed nicely, a bit like a `Matrix`:

```julia
julia> param_collection[1] # Parameter set number 1.
2-element Param{Float64,typeof(model1)}:
 1.0
 2.0

julia> param_collection[1,1] # The first parameter of the first parameter set.
1.0

julia> param_collection[:, 2] # Parameter number 2 for all parameter sets.
2-element Array{Float64,1}:
 2.0
 4.0
```

A parameter collection can also contain parameters for different models:

```julia
julia> model2(x,y,p) = (x/y)^p
julia> p1_model2 = Param(model2, [7])
1-element Param{Int64,typeof(model2)}:
 7

julia> push!(param_collection, p1_model2)
julia> display(param_collection)
Parameter Collection
1: [1.0, 2.0] for model: typeof(model1)
2: [3.0, 4.0] for model: typeof(model1)
3: [7] for model: typeof(model2)
```

Now it pays off to have kept track of which parameters goes with which model since we can use this for filtering:

```julia
julia> param_collection(model1)
Parameter Collection
1: [1.0, 2.0] for model: typeof(model1)
2: [3.0, 4.0] for model: typeof(model1)
```

"calling" a parameter collection (using parenthesis notation) results in filtering which could more verbosely be written as:

```julia
julia> ParamCollection([param for param in param_collection if param.model isa typeof(model1)])
```


### Creating your own parameter type

In the above example we have used the default types that the package supplies. But virtually all of the magic is done by overloads to abstract types. This means that if you define your own type as a subtype of these abstract ones, you will get all of the benefits of the package.


The main type of interest is `AbstractParameter`. All subtypes of this abstract type must have the fields `model` and `params`, but the rest is up to you.
During the creation of new parameter types in this package some intermediate/semi-advanced julia stuff should be adhered to. Since this is a drag, I have made a macro to sort all that out for you.

I often generate my parameters using some optimisation on a cost function. For me the cost value corresponding to the model/parameter combo is often useful. Since it is useful, I want to store it in my parameter type. To do so, I would create my own type using a supplied macro:

```julia
julia> @ParamType MyParamType cost::Number
```

Now the type is created, and I can use it like:

```julia
julia> param = MyParamType(model1, [1., 2.], 1.3)
```

This objects still behaves like a vector, and it can still be placed in a `ParamCollection`. But, now you also have the cost value stored. And, you can use this value for filtering.

```julia
julia> param_collection2 = ParamCollection([
           MyParamType(model1, [1., 2.], 1.3),
           MyParamType(model1, [1.2, 2.], 0.8),
           MyParamType(model1, [1.2, 2.3], 0.3)
           ])
Parameter Collection
1: [1.0, 2.0] for model: typeof(model1)
2: [1.2, 2.0] for model: typeof(model1)
3: [1.2, 2.3] for model: typeof(model1)

julia> ParamCollection([p for p in param_collection2 if p.cost < 1])
Parameter Collection
1: [1.2, 2.0] for model: typeof(model1)
2: [1.2, 2.3] for model: typeof(model1)
```

You get the point. But this too is a little bit verbose for my taste, so I provided a `@filter` macro:

```julia
julia> @filter param_collection2 cost < 1
Parameter Collection
1: [1.2, 2.0] for model: typeof(model1)
2: [1.2, 2.3] for model: typeof(model1)
```

You can also string conditionals together:

```julia
julia> @filter param_collection2 0<cost<1 && param[2]==2.3
Parameter Collection
1: [1.2, 2.3] for model: typeof(model1)
```

This can also be used with timestamps(!):
```julia
julia> using Dates
julia> t1 = now()
julia> t2 = now()
julia> @ParamType TimeParam timestamp::DateTime
julia> model(x, p) = x*p
julia> pc = ParamCollection([
    TimeParam(model, [1], t1 ),
    TimeParam(model, [2], now() )
  ])
Parameter Collection
1: [1] for model: typeof(model)
2: [2] for model: typeof(model)

julia> @filter pc timestamp > t2
Parameter Collection
2: [2] for model: typeof(model)

julia> @filter pc timestamp > DateTime(2018, 09, 04)
Parameter Collection
1: [1] for model: typeof(model)
2: [2] for model: typeof(model)

julia> @filter pc timestamp > DateTime(2018, 09, 05)
Parameter Collection
```

(you can guess when I wrote this example...)

### Conveniences 
If the model you are using has a `params` field (which it does if it was created by DiffEqBiological.jl or ParameterizedFunctions.jl) then you can use the name of a parameter for indexing.

```julia
julia> using DiffEqParameters, DiffEqBiological

julia> model = @reaction_network begin
       (r_b, r_u), a + b ↔ c
       end r_b r_u
(::reaction_network) (generic function with 2 methods)

julia> p = Param(model, [2., 0.5])
2-element Param{reaction_network,Float64}:
  r_b = 2.0
  r_u = 0.5

julia> p[:r_u]
0.5
```

This can also be used for parameter collections. A symbol index to a parameter collection will first look for a matching field and then for a matching parameter. In the end, it will spit out a vector with one element for each parameter set within the collection. 

```julia
julia> pc = ParamCollection([Param(model, [i, rand()]) for i in 1:5])
Parameter Collection with 5 parameter sets.
1: [1.0, 0.06541590104754769] for model: reaction_network
2: [2.0, 0.9888280690818718] for model: reaction_network
3: [3.0, 0.43576942108545813] for model: reaction_network
4: [4.0, 0.41205638736083916] for model: reaction_network
5: [5.0, 0.028078701202775003] for model: reaction_network


julia> pc[:r_u]
5-element Array{Float64,1}:
 0.06541590104754769 
 0.9888280690818718  
 0.43576942108545813 
 0.41205638736083916 
 0.028078701202775003
```

Parameters can also be latexified. 
```julia
julia> using Latexify

julia> latexify(pc[1], fmt = FancyNumberFormatter(2))
```

| $r_{b}$ | $r_{u}$ |
| -------:| -------:|
|     $1$ | $0.065$ |

