"""
    ParamType(TypeName, fieldspec...)

Generate a parameter type with custom fields.


"""
macro ParamType(TypeName, body...)
    expr = quote
        struct $(TypeName){T, T2} <: AbstractParameter{T2}
            model::T
            param::Vector{T2}
            $(body...)
            function $(TypeName){T, T2}(model::T, param::Vector{T2}, $(body...)) where {T, T2}
                if :params in fieldnames(typeof(model))
                    @assert length(param) == length(model.params) "The length of your parameter set does not match what is required from your model (the `params` field of your model)."
                end
                new(model, param, $(body...))
            end
        end
        $(TypeName)(model::T, param::Vector{T2}, $(body...)) where {T, T2} = $(TypeName){T, T2}(model, param, $(body...))
        function $(TypeName)(model::T, $(body...)) where {T}
            if :params in fieldnames(typeof(model))
                $(TypeName)(model, fill(1., length(model.params)), $(body...))
            else
                @error "Cannot infer length of parameter vector for model."
            end
        end
    end
    return esc(expr)
end

## Generate a standard, no frills, Param type called `Param`:
@ParamType Param
