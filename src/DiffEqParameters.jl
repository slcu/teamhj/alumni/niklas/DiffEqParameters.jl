module DiffEqParameters
using DiffEqBase
using MacroTools
using Latexify
import Base: getindex, length, size, iterate, setindex!, push!, append!, show, ==, merge

export Param, ParamCollection, AbstractParameter, AbstractParameterCollection,
    @ParamType, @filter, show

ODEType = Union{DiffEqBase.AbstractParameterizedFunction, DiffEqBase.AbstractReactionNetwork}
include("AbstractParameter.jl")
include("Params.jl")
include("ParamCollection.jl")
include("filtering.jl")
include("latexrecipe.jl")

end # module
